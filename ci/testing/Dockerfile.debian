# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

FROM debian:bookworm-slim
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -qq update && \
	apt-get -qq -y install --no-install-recommends \
	curl \
	etcd-client \
	etcd-server \
	ca-certificates \
	gnupg \
	lsb-release \
	openssh-client \
	openssh-server \
	locales \
	&& rm -rf /var/lib/apt/lists/*
RUN echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -c -s)-pgdg main" \
	| tee /etc/apt/sources.list.d/pgdg.list
RUN curl --output /etc/apt/trusted.gpg.d/pgdg.asc https://www.postgresql.org/media/keys/ACCC4CF8.asc
RUN mkdir -p /etc/postgresql-common/createcluster.d/
RUN echo "create_main_cluster = false" \
	> /etc/postgresql-common/createcluster.d/no-main-cluster.conf
RUN echo deb http://apt.dalibo.org/labs $(lsb_release -cs)-dalibo main > /etc/apt/sources.list.d/dalibo-labs.list
RUN curl https://apt.dalibo.org/labs/debian-dalibo.asc | apt-key add -
RUN apt-get -qq update && \
	apt-get -y install --no-install-recommends postgresql-common
ARG PG_VERSION=17
RUN apt-get -qq update && \
	apt-get -y install --no-install-recommends \
	build-essential \
	git \
	jq \
	procps \
	pipx \
	postgresql-${PG_VERSION} \
	postgresql-${PG_VERSION}-pg-qualstats \
	postgresql-${PG_VERSION}-pg-stat-kcache \
	postgresql-${PG_VERSION}-powa \
	pgbackrest \
	prometheus-postgres-exporter \
	temboard-agent \
	&& rm -rf /var/lib/apt/lists/*


RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && locale-gen

RUN chmod +r /etc/pgbackrest.conf

ARG PG_BACK_VERSION=2.3.0
RUN curl -O -L \
	https://github.com/orgrim/pg_back/releases/download/v${PG_BACK_VERSION}/pg-back_${PG_BACK_VERSION}_linux_amd64.deb
RUN dpkg -i pg-back_${PG_BACK_VERSION}_linux_amd64.deb
RUN rm -rf pg-back_${PG_BACK_VERSION}_linux_amd64.deb

RUN useradd --uid 5432 --create-home runner

RUN touch /etc/port-for.conf
RUN chown runner: /etc/port-for.conf

USER runner
ENV PATH=/home/runner/.local/bin:$PATH
RUN pipx install uv
RUN uv tool install tox --with tox-uv
WORKDIR /home/runner
