# SPDX-FileCopyrightText: 2024 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from typing import Any

import click

from pglift import hba, manager
from pglift.diff import DiffFormat, diff_enabled
from pglift.diff import get as get_diff
from pglift.models import PostgreSQLInstance, interface

from . import model
from .console import console
from .util import (
    Group,
    Obj,
    audit,
    diff_option,
    dry_run_option,
    instance_identifier_option,
    pass_postgresql_instance,
    system_configure,
)


@click.group(cls=Group)
@instance_identifier_option
def cli(**kwargs: Any) -> None:
    """Manage entries in HBA configuration of a PostgreSQL instance."""


@cli.command("add")
@model.as_parameters(interface.HbaRecord, "create")
@pass_postgresql_instance
@dry_run_option
@diff_option
@click.pass_obj
def add(
    obj: Obj,
    instance: PostgreSQLInstance,
    hbarecord: interface.HbaRecord,
    diff: DiffFormat | None,
    dry_run: bool,
) -> None:
    """Add a record in HBA configuration.

    If no --connection-* option is specified, a 'local' record is added.
    """
    with (
        obj.lock,
        audit(dry_run=dry_run),
        diff_enabled(diff),
        system_configure(dry_run=dry_run),
        manager.from_instance(instance),
    ):
        hba.add(instance, hbarecord)
        if (diffvalue := get_diff()) is not None:
            for diffitem in diffvalue:
                console.print(diffitem)


@cli.command("remove")
@model.as_parameters(interface.HbaRecord, "create")
@pass_postgresql_instance
@dry_run_option
@diff_option
@click.pass_obj
def remove(
    obj: Obj,
    instance: PostgreSQLInstance,
    hbarecord: interface.HbaRecord,
    diff: DiffFormat | None,
    dry_run: bool,
) -> None:
    """Remove a record from HBA configuration.

    If no --connection-* option is specified, a 'local' record is removed.
    """
    with (
        obj.lock,
        audit(dry_run=dry_run),
        diff_enabled(diff),
        system_configure(dry_run=dry_run),
        manager.from_instance(instance),
    ):
        hba.remove(instance, hbarecord)
        if (diffvalue := get_diff()) is not None:
            for diffitem in diffvalue:
                console.print(diffitem)
