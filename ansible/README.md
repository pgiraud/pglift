<!--
SPDX-FileCopyrightText: 2022 Dalibo

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Ansible Collection - dalibo.pglift

## Description

This collection provides modules to configure and manage your PostgreSQL
infrastructure based on [pglift](https://pglift.readthedocs.io/); see the
[tutorial](https://pglift.readthedocs.io/en/latest/tutorials/ansible.html).

## Dependencies

To use this collection, pglift must be installed on the managed machine (the
one hosting the PostgreSQL instances you want to manage with the pglift
Ansible collection). For more information about how to install pglift, you can
read and follow the [pglift
documentation](https://pglift.readthedocs.io/en/latest/install.html).

## External requirements

- The pglift executable, in `$PATH`.
- The [PyYAML](https://pypi.org/project/PyYAML/) Python package.

## Tested with Ansible

4.1

## Release notes

See the [change log](https://gitlab.com/dalibo/pglift/-/blob/main/ansible/CHANGELOG.rst).

## Included content

### modules

- `instance`: manage pglift (PostgreSQL) instances
- `database`: manage databases in a PostgreSQL instance
- `role`: manage roles in a PostgreSQL instance
- `postgres_exporter`: manage a Prometheus postgres\_exporter bound to a
  PostgreSQL instance
- `dsn_info`: get libpq environment to connect to a PostgreSQL server instance

## Licensing

The code in this repository is developed and distributed under the GNU General
Public License version 3. See LICENSE for details.
