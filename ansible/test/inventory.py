#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

import json
import subprocess
import sys
from typing import Any


def inventory() -> dict[str, dict[str, Any]]:
    result: dict[str, dict[str, Any]] = {
        "pg": {
            "hosts": [],
        },
        "_meta": {
            "hostvars": {},
        },
    }
    for line in subprocess.check_output(  # nosec
        ["docker-compose", "ps", "--all"], text=True
    ).splitlines()[2:]:
        cname = line.split()[0]
        if cname.startswith("pg"):
            result["pg"]["hosts"].append(cname)
            result["_meta"]["hostvars"][cname] = {
                "ansible_connection": "docker",
                "ansible_user": "pglift",
            }
        else:
            assert cname == "etcd", cname
            result["_meta"]["hostvars"][cname] = {
                "ansible_connection": "docker",
            }
    return result


def main() -> None:
    json.dump(inventory(), sys.stdout)


if __name__ == "__main__":
    main()
