# SPDX-FileCopyrightText: 2022 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path

from ansible_collections.dalibo.pglift.plugins.module_utils.argspec import build_doc


class ModuleDocFragment:
    DOCUMENTATION = build_doc(
        Path(__file__).parent / "database.json", instance_reference=True
    )
