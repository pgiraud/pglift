{
  "examples": "- name: An app database with custom settings, schemas and extensions\n  dalibo.pglift.database:\n    name: app\n    state: present\n    owner: appuser\n    settings:\n      work_mem: 3MB\n    schemas:\n      - name: admin\n        owner: postgres\n    extensions:\n      - name: hstore\n      - name: unaccent\n        version: '1.1'\n\n- name: A database cloned and migrated through logical replication\n  dalibo.pglift.database:\n    name: migrated-app\n    clone:\n      dsn: postgresql://app:{{ app_password }}@localhost:5499/app\n      schema_only: true\n    subscriptions:\n      - name: migrate\n        enabled: false\n        connection:\n          conninfo: host=localhost user=app port=5499\n          password: '{{ app_password }}'\n        publications:\n          - migrate\n",
  "options": {
    "clone": {
      "description": [
        "Options for cloning a database into this one"
      ],
      "options": {
        "dsn": {
          "description": [
            "Data source name of the database to restore into this one, specified as a libpq connection URI"
          ],
          "required": true,
          "type": "str"
        },
        "schema_only": {
          "default": false,
          "description": [
            "Only restore the schema (data definitions)"
          ],
          "type": "bool"
        }
      },
      "type": "dict"
    },
    "extensions": {
      "default": [],
      "description": [
        "Extensions in this database"
      ],
      "elements": "dict",
      "options": {
        "name": {
          "description": [
            "Extension name"
          ],
          "required": true,
          "type": "str"
        },
        "schema": {
          "description": [
            "Name of the schema in which to install the extension's object"
          ],
          "type": "str"
        },
        "state": {
          "choices": [
            "present",
            "absent"
          ],
          "default": "present",
          "description": [
            "Extension state"
          ]
        },
        "version": {
          "description": [
            "Version of the extension to install"
          ],
          "type": "str"
        }
      },
      "type": "list"
    },
    "force_drop": {
      "default": false,
      "description": [
        "Force the drop"
      ],
      "type": "bool"
    },
    "locale": {
      "description": [
        "Locale for this database",
        "Database will be created from template0 if the locale differs from the one set for template1"
      ],
      "type": "str"
    },
    "name": {
      "description": [
        "Database name"
      ],
      "required": true,
      "type": "str"
    },
    "owner": {
      "description": [
        "The role name of the user who will own the database"
      ],
      "type": "str"
    },
    "publications": {
      "default": [],
      "description": [
        "Publications in this database"
      ],
      "elements": "dict",
      "options": {
        "name": {
          "description": [
            "Name of the publication, unique in the database"
          ],
          "required": true,
          "type": "str"
        },
        "state": {
          "choices": [
            "present",
            "absent"
          ],
          "default": "present",
          "description": [
            "Presence state"
          ]
        }
      },
      "type": "list"
    },
    "schemas": {
      "default": [],
      "description": [
        "Schemas in this database"
      ],
      "elements": "dict",
      "options": {
        "name": {
          "description": [
            "Schema name"
          ],
          "required": true,
          "type": "str"
        },
        "owner": {
          "description": [
            "The role name of the user who will own the schema"
          ],
          "type": "str"
        },
        "state": {
          "choices": [
            "present",
            "absent"
          ],
          "default": "present",
          "description": [
            "Schema state"
          ]
        }
      },
      "type": "list"
    },
    "settings": {
      "description": [
        "Session defaults for run-time configuration variables for the database",
        "Upon update, an empty (dict) value would reset all settings"
      ],
      "required": false,
      "type": "dict"
    },
    "state": {
      "choices": [
        "present",
        "absent"
      ],
      "default": "present",
      "description": [
        "Database state"
      ]
    },
    "subscriptions": {
      "default": [],
      "description": [
        "Subscriptions in this database"
      ],
      "elements": "dict",
      "options": {
        "connection": {
          "description": [
            "The libpq connection string defining how to connect to the publisher database"
          ],
          "options": {
            "conninfo": {
              "description": [
                "The libpq connection string, without password"
              ],
              "required": true,
              "type": "str"
            },
            "password": {
              "description": [
                "Optional password to inject into the connection string"
              ],
              "no_log": true,
              "type": "str"
            }
          },
          "required": true,
          "type": "dict"
        },
        "enabled": {
          "default": true,
          "description": [
            "Enable or disable the subscription"
          ],
          "type": "bool"
        },
        "name": {
          "description": [
            "Name of the subscription"
          ],
          "required": true,
          "type": "str"
        },
        "publications": {
          "description": [
            "Publications on the publisher to subscribe to"
          ],
          "elements": "str",
          "required": true,
          "type": "list"
        },
        "state": {
          "choices": [
            "present",
            "absent"
          ],
          "default": "present",
          "description": [
            "Presence state"
          ]
        }
      },
      "type": "list"
    },
    "tablespace": {
      "description": [
        "The name of the tablespace that will be associated with the database"
      ],
      "type": "str"
    }
  },
  "return values": {
    "change_state": {
      "choices": [
        "created",
        "changed",
        "dropped"
      ],
      "description": [
        "Define the change applied (created, changed or dropped) to a manipulated object"
      ]
    },
    "diff": {
      "description": [
        "Changes on the filesystem resulting of applied operation",
        "Only set if diff computation was requested"
      ]
    }
  }
}
