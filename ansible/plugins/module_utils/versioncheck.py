# SPDX-FileCopyrightText: 2022 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later


from __future__ import annotations

from ansible.module_utils.basic import AnsibleModule

PGLIFT_REQUIRED_MIN_VERSION: tuple[int, int] = (1, 0)


def check_pglift_version(module: AnsibleModule) -> None:
    _, stdout, _ = module.run_command(["pglift", "--version"], check_rc=True)
    version = tuple(
        int(c)
        for c in stdout.split(" ")[-1].split(".")[: len(PGLIFT_REQUIRED_MIN_VERSION)]
    )
    msg = _check(version, PGLIFT_REQUIRED_MIN_VERSION)
    if msg:
        module.fail_json(msg)


def _check(version: tuple[int, ...], min_version: tuple[int, int]) -> str | None:
    """Check if 'version' is compatible with pglift requirements.

    >>> _check((1, 3), (1, 1))
    >>> _check((0, 1), (1, 1))
    'Version of pglift ((0, 1)) not supported; expecting >= (1, 1), < 2'
    >>> _check((2, 3), (1, 1))
    'Version of pglift ((2, 3)) not supported; expecting >= (1, 1), < 2'
    """
    major, _ = min_version
    if min_version <= version < (major + 1,):
        return None
    return f"Version of pglift ({version}) not supported; expecting >= {min_version}, < {major + 1}"
