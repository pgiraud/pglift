# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import itertools
import logging
import shutil
import tempfile
from collections.abc import Iterator
from pathlib import Path
from typing import Any
from unittest.mock import patch

import port_for
import pytest
from trustme import CA

from . import CertFactory, Certificate
from .etcd import Etcd

default_pg_version: str | None
try:
    from pglift.settings._postgresql import _postgresql_bindir_version

    default_pg_version = _postgresql_bindir_version()[1]
except (ImportError, OSError):
    default_pg_version = None


def pytest_addoption(parser: Any) -> None:
    try:
        from pglift.settings._postgresql import PostgreSQLVersion
    except ImportError:
        pass
    else:
        parser.addoption(
            "--pg-version",
            choices=list(PostgreSQLVersion),
            help="Run tests with specified PostgreSQL version (default: %(default)s)",
        )
    parser.addoption(
        "--no-plugins",
        action="store_true",
        default=False,
        help="Run tests without any pglift plugin loaded.",
    )


def pytest_report_header(config: Any) -> list[str]:
    try:
        pg_version = config.option.pg_version or default_pg_version
    except AttributeError:
        return []
    return [f"postgresql: {pg_version}"]


@pytest.fixture(scope="session")
def anyio_backend() -> str:
    return "asyncio"


@pytest.fixture(scope="session")
def no_plugins(request: Any) -> bool:
    value = request.config.option.no_plugins
    assert isinstance(value, bool)
    return value


@pytest.fixture
def datadir() -> Path:
    return Path(__file__).parent / "data"


@pytest.fixture(scope="session")
def logger() -> logging.Logger:
    return logging.getLogger("pglift-tests")


@pytest.fixture(scope="session", autouse=True)
def _log_level(logger: logging.Logger) -> None:
    logging.getLogger("pglift").setLevel(logging.DEBUG)
    logger.setLevel(logging.DEBUG)


@pytest.fixture(scope="package")
def config_dir(tmp_path_factory: pytest.TempPathFactory) -> Path:
    """An empty PGLIFT_CONFIG_DIR, to prevent usage of user's site configuration."""
    return tmp_path_factory.mktemp("pglift-config-dir")


@pytest.fixture(autouse=True, scope="package")
def _pglift_env(config_dir: Path) -> Iterator[None]:
    """Setup pglift environment (e.g. PGLIFT_CONFIG_DIR) for tests."""
    with patch.dict("os.environ", {"PGLIFT_CONFIG_DIR": str(config_dir)}):
        yield


@pytest.fixture(scope="session")
def pg_bindir(request: Any) -> tuple[Path, str]:
    from pglift.settings._postgresql import _postgresql_bindir

    # --pg-version option should be available at this point
    version = request.config.option.pg_version or default_pg_version
    if version is None:
        pytest.skip("no PostgreSQL installation found")
    assert isinstance(version, str)
    bindir = _postgresql_bindir()
    assert bindir is not None  # otherwise, version would be None too.
    return Path(bindir.format(version=version)), version


@pytest.fixture(scope="session")
def pg_version(pg_bindir: tuple[Path, str]) -> str:
    return pg_bindir[1]


@pytest.fixture(scope="session")
def pgbackrest_execpath(no_plugins: bool) -> Path | None:
    if no_plugins:
        return None
    if (path := shutil.which("pgbackrest")) is not None:
        return Path(path)
    return None


@pytest.fixture(scope="session")
def pgbackrest_available(pgbackrest_execpath: Path | None) -> bool:
    return pgbackrest_execpath is not None


@pytest.fixture(scope="session")
def pg_back_execpath(no_plugins: bool) -> Path | None:
    if no_plugins:
        return None
    if (path := shutil.which("pg_back")) is not None:
        return Path(path)
    return None


@pytest.fixture(scope="session")
def prometheus_execpath(no_plugins: bool) -> Path | None:
    if no_plugins:
        return None
    for name in ("prometheus-postgres-exporter", "postgres_exporter"):
        path = shutil.which(name)
        if path is not None:
            return Path(path)
    return None


@pytest.fixture(scope="session")
def temboard_execpath(no_plugins: bool) -> Path | None:
    if no_plugins:
        return None
    path = shutil.which("temboard-agent")
    if path is not None:
        return Path(path)
    return None


@pytest.fixture(scope="session")
def patroni_execpaths(no_plugins: bool) -> tuple[Path, Path] | None:
    if no_plugins:
        return None
    patroni, patronictl = shutil.which("patroni"), shutil.which("patronictl")
    if patroni is not None and patronictl is not None:
        return Path(patroni), Path(patronictl)
    return None


@pytest.fixture(scope="package")
def tmp_port_factory() -> Iterator[int]:
    """Return a generator producing available and distinct TCP ports."""

    def available_ports() -> Iterator[int]:
        used: set[int] = set()
        while True:
            port = port_for.select_random(exclude_ports=list(used))
            used.add(port)
            yield port

    return available_ports()


@pytest.fixture(scope="package")
def etcd_host(
    tmp_path_factory: pytest.TempPathFactory,
    tmp_port_factory: Iterator[int],
    ca: CA,
) -> Etcd | None:
    if (p := shutil.which("etcd")) is None:
        return None
    execdir = Path(p).parent
    if not (execdir / "etcdctl").exists():
        pytest.skip("etcdctl executable not found")
    return Etcd(
        execdir=execdir,
        name="pglift-tests",
        basedir=tmp_path_factory.mktemp("etcd"),
        client_port=next(tmp_port_factory),
        peer_port=next(tmp_port_factory),
        ca=ca,
    )


@pytest.fixture(scope="package")
def ca(tmp_path_factory: pytest.TempPathFactory) -> CA:
    return CA(organization_name="dalibo", organization_unit_name="pglift")


@pytest.fixture(scope="package")
def _ssldir(tmp_path_factory: pytest.TempPathFactory) -> Path:
    return tmp_path_factory.mktemp("ssl")


@pytest.fixture(scope="package")
def ca_cert(_ssldir: Path, ca: CA) -> Path:
    p = _ssldir / "root.crt"
    ca.cert_pem.write_to_path(p)
    return p


@pytest.fixture(scope="package")
def ca_private_key(_ssldir: Path, ca: CA) -> Path:
    p = _ssldir / "root.key"
    p.touch(mode=0o600)
    ca.private_key_pem.write_to_path(p)
    return p


@pytest.fixture(scope="package")
def cert_factory(ca: CA, _ssldir: Path) -> CertFactory:
    itertools.count()

    def factory(*identities: str, common_name: str | None = None) -> Certificate:
        cert = ca.issue_cert(*identities, common_name=common_name)
        with tempfile.NamedTemporaryFile(
            dir=_ssldir, delete=False, suffix=".pem"
        ) as certfile:
            certfile.write(cert.cert_chain_pems[0].bytes())
        with tempfile.NamedTemporaryFile(
            dir=_ssldir, delete=False, suffix=".pem"
        ) as keyfile:
            keyfile.write(cert.private_key_pem.bytes())
        return Certificate(path=Path(certfile.name), private_key=Path(keyfile.name))

    return factory
