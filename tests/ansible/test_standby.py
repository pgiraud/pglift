# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import json
import os
import subprocess
from collections.abc import Callable, Iterator
from pathlib import Path
from typing import Any

import psycopg
import pytest
import yaml

from . import Play, convert_for_pydantic_envvar


@pytest.fixture
def postgresql_socket_directory(tmp_path: Path) -> Path:
    return tmp_path / "pgsql"


@pytest.fixture
def site_settings_env(
    tmp_path: Path, postgresql_socket_directory: Path
) -> Iterator[dict[str, str]]:
    settings = {
        "prefix": str(tmp_path),
        "postgresql": {
            "socket_directory": str(postgresql_socket_directory),
            "replrole": "replication",
        },
    }
    env = convert_for_pydantic_envvar(settings)
    subprocess.run(
        ["pglift", "--debug", "site-configure", "install"],
        capture_output=True,
        check=True,
        env=os.environ | env,
    )
    yield env
    subprocess.run(
        ["pglift", "--non-interactive", "--debug", "site-configure", "uninstall"],
        capture_output=True,
        check=True,
        env=os.environ | env,
    )


@pytest.fixture
def replication_password() -> str:
    return "r3pl1c@t"


@pytest.fixture
def call_playbook(
    tmp_path: Path,
    site_settings_env: dict[str, str],
    ansible_env: dict[str, str],
    ansible_vault: Callable[[dict[str, str]], Path],
    replication_password: str,
) -> Iterator[Play[Path]]:
    vault = ansible_vault({"replication_role_password": replication_password})
    teardown_play = tmp_path / "teardown.yml"
    with teardown_play.open("w") as f:
        yaml.safe_dump(
            [
                {
                    "name": "teardown standby setup",
                    "hosts": "localhost",
                    "tasks": [
                        {
                            "name": "drop standby",
                            "dalibo.pglift.instance": {
                                "name": "pg2",
                                "state": "absent",
                            },
                        },
                        {
                            "name": "drop primary",
                            "dalibo.pglift.instance": {
                                "name": "pg1",
                                "state": "absent",
                            },
                        },
                    ],
                }
            ],
            f,
        )

    def call(playfile: Path, /, **extra_vars: Any) -> None:
        vars_path = tmp_path / f"{playfile.stem}.json"
        with vars_path.open("w") as f:
            json.dump(extra_vars, f)
        env = (
            os.environ
            | ansible_env
            | site_settings_env
            | {"PGLIFT_CONFIG_DIR": str(Path(__file__).parent / "data")}
        )
        subprocess.check_call(
            [
                "ansible-playbook",
                "--extra-vars",
                f"@{vault}",
                "--extra-vars",
                f"@{vars_path}",
                playfile,
            ],
            env=env,
        )

    yield call
    call(teardown_play)


def test(
    playdir: Path,
    call_playbook: Play[Path],
    postgresql_socket_directory: Path,
    replication_password: str,
    tmp_port_factory: Iterator[int],
) -> None:
    ports = {env: {"port": next(tmp_port_factory)} for env in ("pg1", "pg2")}
    call_playbook(playdir / "standby-setup.yml", **ports)

    host = str(postgresql_socket_directory)
    pg1_port, pg2_port = ports["pg1"]["port"], ports["pg2"]["port"]

    # pg2 is the standby
    with psycopg.connect(
        port=pg2_port,
        host=host,
        user="replication",
        password=replication_password,
        dbname="postgres",
    ) as conn:
        rows = conn.execute("SELECT * FROM pg_is_in_recovery()").fetchall()
    assert rows == [(True,)]
    # replication uses a slot (on the primary)
    with psycopg.connect(port=pg1_port, host=host, user="postgres") as conn:
        assert conn.execute(
            "SELECT slot_name FROM pg_replication_slots WHERE active IS true"
        ).fetchall() == [("replislot",)]

    call_playbook(playdir / "standby-promote.yml", **ports)

    # pg1 is the new standby
    with psycopg.connect(
        port=pg1_port,
        host=host,
        user="replication",
        password=replication_password,
        dbname="postgres",
    ) as conn:
        rows = conn.execute("SELECT * FROM pg_is_in_recovery()").fetchall()
    assert rows == [(True,)]
    # pg2 is the new primary
    with psycopg.connect(port=pg2_port, host=host, user="postgres") as conn:
        rows = conn.execute("SELECT * FROM pg_is_in_recovery()").fetchall()
        assert rows == [(False,)]
        # replication uses the new slot
        assert conn.execute(
            "SELECT slot_name FROM pg_replication_slots WHERE active IS true"
        ).fetchall() == [("newslot",)]
