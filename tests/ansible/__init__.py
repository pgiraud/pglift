# SPDX-FileCopyrightText: 2024 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import json
from collections.abc import Mapping
from pathlib import Path
from typing import Any, Protocol, TypeVar

T = TypeVar("T", str, Path, contravariant=True)


class Play(Protocol[T]):
    def __call__(self, value: T, /, **extra_vars: Any) -> None: ...


def convert_for_pydantic_envvar(settings: dict[str, Any]) -> dict[str, str]:
    return {
        f"PGLIFT_{k}": json.dumps(v)
        if isinstance(v, Mapping | list | type(None))
        else str(v)
        for k, v in settings.items()
    }
