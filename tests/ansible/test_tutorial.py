# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import datetime
import json
import os
import socket
import subprocess
from collections.abc import Callable, Iterator
from pathlib import Path
from typing import Any

import psycopg
import pytest
import yaml
from psycopg.rows import dict_row

from .. import CertFactory
from . import Play, convert_for_pydantic_envvar


@pytest.fixture
def site_settings_env(
    tmp_path: Path,
    playdir: Path,
    pgbackrest_execpath: Path | None,
    prometheus_execpath: Path | None,
    ca_cert: Path,
    cert_factory: CertFactory,
) -> Iterator[dict[str, str]]:
    with (playdir / "tutorial-settings.yaml.j2").open() as f:
        settings = yaml.safe_load(f)

    settings |= {
        "prefix": str(tmp_path),
        "run_prefix": str(tmp_path / "run"),
    }
    settings["postgresql"]["auth"]["passfile"] = str(tmp_path / "pgpass")
    if pgbackrest_execpath is not None:
        settings["pgbackrest"] |= {"execpath": str(pgbackrest_execpath)}
        settings["pgbackrest"]["repository"]["path"] = str(tmp_path / "pgbackups")
    else:
        pytest.skip("pgbackrest not available")
    if prometheus_execpath:
        settings["prometheus"] |= {"execpath": str(prometheus_execpath)}
    else:
        pytest.skip("prometheus not available")
    env = convert_for_pydantic_envvar(settings)
    subprocess.run(
        ["pglift", "--debug", "site-configure", "install"],
        capture_output=True,
        check=True,
        env=os.environ | env,
    )
    yield env
    subprocess.run(
        ["pglift", "--non-interactive", "--debug", "site-configure", "uninstall"],
        capture_output=True,
        check=True,
        env=os.environ | env,
    )


@pytest.fixture
def call_playbook(
    tmp_path: Path,
    ansible_env: dict[str, str],
    site_settings_env: dict[str, str],
    ansible_vault: Callable[[dict[str, str]], Path],
    playdir: Path,
) -> Iterator[Play[str]]:
    env = os.environ | ansible_env | site_settings_env
    vault = ansible_vault(
        {
            "postgresql_surole_password": "supers3kret",
            "prod_bob_password": "s3kret",
            "backup_role_password": "b4ckup",
            "prometheus_role_password": "pr0m3th3u$",
        }
    )

    def call(playname: str, /, **extra_vars: Any) -> None:
        vars_path = tmp_path / f"{playname}.json"
        with vars_path.open("w") as f:
            json.dump(extra_vars, f)
        subprocess.check_call(
            [
                "ansible-playbook",
                "--extra-vars",
                f"@{vault}",
                "--extra-vars",
                f"@{vars_path}",
                playdir / playname,
            ],
            env=env,
        )

    yield call

    call("instances-delete.yml")


def test_validationerror(tmp_path: Path, site_settings_env: dict[str, str]) -> None:
    play = tmp_path / "invalid.yml"
    with play.open("w") as f:
        yaml.safe_dump(
            [
                {
                    "hosts": "localhost",
                    "tasks": [
                        {
                            "name": "invalid input",
                            "dalibo.pglift.instance": {
                                "name": "invalid",
                                "settings": {"port": "5555"},
                                "pgbackrest": {"stanza": "whatever"},
                            },
                        },
                    ],
                }
            ],
            f,
        )
    with pytest.raises(subprocess.CalledProcessError) as cm:
        subprocess.run(
            ["ansible-playbook", play],
            env=(
                os.environ
                | site_settings_env
                | {
                    "ANSIBLE_CALLBACK_ENABLED": "ansible.posix.json",
                    "ANSIBLE_STDOUT_CALLBACK": "ansible.posix.json",
                }
            ),
            check=True,
            capture_output=True,
            text=True,
        )
    output = json.loads(cm.value.stdout)
    assert {
        "input": {"port": "5555"},
        "loc": ["settings"],
        "msg": "Value error, 'port' entry is disallowed; use the main 'port' field",
        "type": "value_error",
    } in output["plays"][0]["tasks"][1]["hosts"]["localhost"]["errors"]


def cluster_name(dsn: str) -> str:
    with psycopg.connect(dsn, row_factory=dict_row) as cnx:
        cur = cnx.execute("SELECT setting FROM pg_settings WHERE name = 'cluster_name'")
        row = cur.fetchone()
        assert row
        name = row["setting"]
        assert isinstance(name, str), name
        return name


def test(call_playbook: Play[str], tmp_port_factory: Iterator[int]) -> None:
    ports = {
        env: {
            "postgresql_port": next(tmp_port_factory),
            "prometheus_port": next(tmp_port_factory),
        }
        for env in ("prod", "preprod", "dev")
    }
    call_playbook("instances-create.yml", **ports)

    prod_pgport = ports["prod"]["postgresql_port"]
    prod_dsn = f"host=/tmp user=postgres password=supers3kret dbname=postgres port={prod_pgport}"
    assert cluster_name(prod_dsn) == "prod"
    with psycopg.connect(prod_dsn, row_factory=dict_row) as cnx:
        cnx.execute("SET TIME ZONE 'UTC'")
        cur = cnx.execute(
            "SELECT rolname,rolinherit,rolcanlogin,rolconnlimit,rolpassword,rolvaliduntil FROM pg_roles WHERE rolname = 'bob'"
        )
        assert cur.fetchone() == {
            "rolname": "bob",
            "rolinherit": True,
            "rolcanlogin": True,
            "rolconnlimit": 10,
            "rolpassword": "********",
            "rolvaliduntil": datetime.datetime(
                2099, 1, 1, tzinfo=datetime.timezone.utc
            ),
        }
        cur = cnx.execute(
            "SELECT r.rolname AS role, ARRAY_AGG(m.rolname ORDER BY m.rolname) AS member_of FROM pg_auth_members JOIN pg_authid m ON pg_auth_members.roleid = m.oid JOIN pg_authid r ON pg_auth_members.member = r.oid GROUP BY r.rolname"
        )
        assert cur.fetchall() == [
            {"role": "bob", "member_of": ["pg_read_all_stats", "pg_signal_backend"]},
            {"role": "peter", "member_of": ["pg_signal_backend"]},
            {
                "role": "pg_monitor",
                "member_of": [
                    "pg_read_all_settings",
                    "pg_read_all_stats",
                    "pg_stat_scan_tables",
                ],
            },
            {"role": "prometheus", "member_of": ["pg_monitor"]},
            {"role": "replication", "member_of": ["pg_read_all_stats"]},
        ]
        libraries = cnx.execute(
            "SELECT setting FROM pg_settings WHERE name = 'shared_preload_libraries';"
        ).fetchone()

        cur = cnx.execute(
            "SELECT type, database, user_name, address, netmask, auth_method FROM pg_hba_file_rules WHERE 'alice' = ANY(user_name)"
        )
        assert cur.fetchall() == [
            {
                "address": "192.168.0.0",
                "auth_method": "trust",
                "database": [
                    "db",
                ],
                "netmask": "255.255.0.0",
                "type": "host",
                "user_name": [
                    "alice",
                ],
            },
            {
                "address": "192.168.1.0",
                "auth_method": "trust",
                "database": [
                    "db2",
                ],
                "netmask": "255.255.255.0",
                "type": "host",
                "user_name": [
                    "alice",
                ],
            },
            {
                "address": "samehost",
                "auth_method": "trust",
                "database": [
                    "db2",
                ],
                "netmask": None,
                "type": "host",
                "user_name": [
                    "alice",
                ],
            },
        ]

    assert libraries is not None
    assert "pg_stat_statements" in libraries["setting"]

    prod_pgeport = ports["prod"]["prometheus_port"]
    socket.create_connection(("127.0.0.1", prod_pgeport), 1).close()

    # test connection with bob to the db database
    with psycopg.connect(
        f"host=/tmp user=bob password=s3kret dbname=db port={prod_pgport}",
        row_factory=dict_row,
    ) as cnx:
        row = cnx.execute("SHOW work_mem").fetchone()
        extensions = cnx.execute("SELECT extname FROM pg_extension").fetchall()
    assert row is not None
    assert row["work_mem"] == "3MB"
    installed = [r["extname"] for r in extensions]
    assert "unaccent" in installed

    # check preprod cluster & postgres_exporter
    preprod_pgport = ports["preprod"]["postgresql_port"]
    preprod_dsn = f"host=/tmp user=postgres password=supers3kret dbname=test port={preprod_pgport}"
    preprod_pgeport = ports["preprod"]["prometheus_port"]
    assert cluster_name(preprod_dsn) == "preprod"
    socket.create_connection(("127.0.0.1", preprod_pgeport), 1).close()

    # check dev cluster and postgres_exporter are stopped
    dev_pgport = ports["dev"]["postgresql_port"]
    with pytest.raises(psycopg.OperationalError, match="No such file or directory"):
        cluster_name(
            f"host=/tmp user=postgres password=supers3kret dbname=postgres port={dev_pgport}"
        )
    dev_pgeport = ports["dev"]["prometheus_port"]
    with pytest.raises(ConnectionRefusedError):
        socket.create_connection(("127.0.0.1", dev_pgeport), 1)

    call_playbook("instances-update.yml", **ports)

    # prod running
    assert cluster_name(prod_dsn) == "prod"

    # pg_stat_statements extension is uninstalled
    with psycopg.connect(prod_dsn, row_factory=dict_row) as cnx:
        libraries = cnx.execute(
            "SELECT setting FROM pg_settings WHERE name = 'shared_preload_libraries';"
        ).fetchone()
    assert libraries is not None
    assert "pg_stat_statements" not in libraries["setting"]

    # bob user and db database no longer exists
    with pytest.raises(
        psycopg.OperationalError, match='password authentication failed for user "bob"'
    ):
        with psycopg.connect(
            f"host=/tmp user=bob password=s3kret dbname=template1 port={prod_pgport}"
        ):
            pass
    with pytest.raises(psycopg.OperationalError, match='database "db" does not exist'):
        with psycopg.connect(
            f"host=/tmp user=postgres password=supers3kret dbname=db port={prod_pgport}"
        ):
            pass
    with pytest.raises(psycopg.OperationalError, match='database "db2" does not exist'):
        with psycopg.connect(
            f"host=/tmp user=postgres password=supers3kret dbname=db2 port={prod_pgport}"
        ):
            pass
    with psycopg.connect(prod_dsn, row_factory=dict_row) as cnx:
        row = cnx.execute(
            "SELECT pg_catalog.pg_get_userbyid(d.datdba) as owner FROM pg_catalog.pg_database d WHERE d.datname = 'db3'"
        ).fetchone()
    assert row is not None
    assert row["owner"] == "postgres"

    # Entries in pg_hba have changed for alice
    with psycopg.connect(prod_dsn, row_factory=dict_row) as cnx:
        cur = cnx.execute(
            "SELECT type, database, user_name, address, netmask, auth_method FROM pg_hba_file_rules WHERE 'alice' = ANY(user_name)"
        )
        assert cur.fetchall() == [
            {
                "address": "samehost",
                "auth_method": "trust",
                "database": [
                    "db2",
                ],
                "netmask": None,
                "type": "host",
                "user_name": [
                    "alice",
                ],
            },
        ]

    # preprod stopped
    with pytest.raises(psycopg.OperationalError, match="No such file or directory"):
        assert cluster_name(preprod_dsn) == "preprod"
    with pytest.raises(ConnectionRefusedError):
        socket.create_connection(("127.0.0.1", preprod_pgeport), 1)

    # dev running
    dev_dsn = f"host=/tmp user=postgres password=supers3kret dbname=postgres port={dev_pgport}"
    assert cluster_name(dev_dsn) == "dev"
    socket.create_connection(("127.0.0.1", dev_pgeport)).close()
