# SPDX-FileCopyrightText: 2024 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

-e ./lib[dev]
-e ./cli[dev]
-r lint.txt
-r ../docs/requirements.txt
pip-tools
pytest-accept
pytest-pretty
sphinx-autobuild
