Fix Patroni configuration validation error when templated ``pg_ident.conf`` is empty.
