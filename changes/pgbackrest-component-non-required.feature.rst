Make the ``pgbackrest`` satellite component of instance non-required even if
enabled in site settings. The direct consequence is that the
``--pgbackrest-stanza`` command-line option (of ``pgbackrest.stanza`` field in
Ansible) is no longer required. When not specified, it is now possible to
create and manipulate instances without pgBackRest being configured.
