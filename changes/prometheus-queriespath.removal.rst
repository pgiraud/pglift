Setting ``prometheus.queriespath`` got removed as support for
``extend.query-path`` option in `postgres_exporter 0.13
<https://github.com/prometheus-community/postgres_exporter/releases/tag/v0.13.0>`_
is deprecated. The previous (empty) YAML file containing custom queries for
postgres_exporter is no longer installed. In the future, support for a
dedicated sql_exporter will be added to provide equivalent features.
