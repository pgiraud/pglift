Prevent failures upon "de-init" of PostgreSQL instance, during ``pg_ctl status``
check, when the initially requested operation fails.
