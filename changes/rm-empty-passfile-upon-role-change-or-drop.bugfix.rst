Remove the "pass file" if left empty by a role change or drop, consistently
with what already happens at instance drop.
