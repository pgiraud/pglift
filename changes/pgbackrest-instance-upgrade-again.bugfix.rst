Make it possible to upgrade again an instance that has already been upgraded,
especially if pgBackRest was configured on the instance.
