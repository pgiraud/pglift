Environment variables without the ``pglift_`` prefix are now unsupported.
Prefixed variables should be used instead (Eg: ``postgresql`` variable should
be replaced by ``pglift_postgresql``).
