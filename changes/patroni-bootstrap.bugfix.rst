Remove useless ``pg_hba`` and ``pg_ident`` entries from ``bootstrap`` section
in Patroni configuration file.
