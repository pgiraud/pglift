SSL authentication for replication and rewind can now be updated for the
instances managed by Patroni. Corresponding options are now available in
the ``instance alter`` command.
