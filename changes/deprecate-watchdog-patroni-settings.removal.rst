Deprecate the ``watchdog`` settings.

``watchdog`` configuration should be done by overriding
the Patroni template.
