.. SPDX-FileCopyrightText: 2021 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Privileges
==========

.. currentmodule:: pglift.privileges

Module :mod:`pglift.privileges` exposes the following API to manipulate
access privileges:

.. autofunction:: get
