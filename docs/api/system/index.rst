.. SPDX-FileCopyrightText: 2024 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Operational interface to the underlying system
==============================================

.. toctree::
    :maxdepth: 1

    cmd
