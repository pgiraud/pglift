.. SPDX-FileCopyrightText: 2021 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. currentmodule:: pglift.system.cmd

Command execution
=================

.. autofunction:: run
.. autofunction:: start_program
.. autofunction:: terminate_program
.. autofunction:: status_program
