.. SPDX-FileCopyrightText: 2021 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Databases
=========

.. currentmodule:: pglift.databases

Module :mod:`pglift.databases` exposes the following API to manipulate
PostgreSQL databases:

.. autofunction:: apply
.. autofunction:: exists
.. autofunction:: ls
.. autofunction:: get
.. autofunction:: drop
.. autofunction:: run
.. autofunction:: dump
