.. SPDX-FileCopyrightText: 2021 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. currentmodule:: pglift.conf

PostgreSQL configuration management
===================================

.. autofunction:: make
.. autofunction:: read
