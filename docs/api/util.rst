.. SPDX-FileCopyrightText: 2024 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. currentmodule:: pglift.util

Utilities
=========

.. autofunction:: template
