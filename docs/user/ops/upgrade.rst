.. SPDX-FileCopyrightText: 2024 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. _instance-upgrade:

Upgrade
=======

The ``pglift instance`` command line entry point exposes an ``upgrade``
command which makes the upgrade of an instance from a version of PostgreSQL
to a more recent one easy.

Assuming we have a ``main`` instance running with version *14*:

.. code-block:: console

    $ pglift instance status main
    PostgreSQL: running

    $ pglift instance get main
     name  version  port  data_checksums  locale  encoding  pending_restart
     main  14       5433  False           C       UTF8      False

.. note::
    Before running an upgrade, it is safer to :ref:`backup <instance-backup>`
    the instance.

First, the old instance needs to be stopped.

.. code-block:: console

    $ pglift instance stop main
    INFO     stopping PostgreSQL 14/main

Then we can upgrade the instance. Let's consider we want to upgrade to the
version *16*.

.. code-block:: console

    $ pglift instance upgrade main --version=16
    > Confirm upgrade of instance 14/main to version 16? [y/n] (y):
    INFO     upgrading instance 14/main as 16/main
    INFO     initializing PostgreSQL
    INFO     configuring PostgreSQL authentication
    INFO     configuring PostgreSQL
    INFO     upgrading instance with pg_upgrade
    INFO     configuring PostgreSQL
    INFO     creating instance dumps directory: /srv/pglift/srv/dumps/16-main
    INFO     starting PostgreSQL 16/main

A new instance is created along with the older stopped one.

.. code-block:: console

    $ pglift instance list
    ┏━━━━━━┳━━━━━━━━━┳━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━┓
    ┃ name ┃ version ┃ port ┃ datadir                            ┃ status      ┃
    ┡━━━━━━╇━━━━━━━━━╇━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━┩
    │ main │ 14      │ 5433 │ /srv/pglift/srv/pgsql/14/main/data │ not_running │
    │ main │ 16      │ 5433 │ /srv/pglift/srv/pgsql/16/main/data │ running     │
    └──────┴─────────┴──────┴────────────────────────────────────┴─────────────┘

Without any provided options, the upgraded instance uses the same port. But
it's possible to use a different one by using the ``--port`` option.
A different name can as well be chosen with the ``--name`` option.

pglift will use the latest available version as target unless we provide
one explicitly with the ``--version`` option.

.. note::

    Behind the scene, *pglift* relies on the ``pg_upgrade`` command.

.. note::

    If ``datadir`` and ``waldir`` settings in the ``postgresql`` section are
    defined without a ``{version}`` template variable, a name must be explicitly
    specified for the new instance (through ``--name`` option).

.. seealso::

   :ref:`patroni-cluster-upgrade` of Patroni-managed instances.
