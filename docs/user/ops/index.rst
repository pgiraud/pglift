.. SPDX-FileCopyrightText: 2021 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Operations
==========

This section describes day-to-day operations using pglift through available
interfaces (command-line, Ansible, Python).

.. toctree::
    :maxdepth: 2

    instance
    postgresql-configuration
    roles
    databases
    privileges
    profiles
    backup
    ha
    upgrade
